FROM ubuntu:latest

RUN apt-get update && apt-get install -y cron wget nodejs npm zip coreutils
RUN cd /root/ && npm install node-trello
 
# Add crontab file in the cron directory
ADD crontab /etc/cron.d/hello-cron
 
# Give execution rights on the cron job
RUN chmod 0644 /etc/cron.d/hello-cron
 
# Create the log file to be able to run tail
RUN touch /var/log/cron.log
 
ADD download.js /root/download.js
Add main.sh /root/
Add TrelloDownloader.sh /root/
Add createCron.sh /root/

# Run the command on container startup
CMD /root/createCron.sh
