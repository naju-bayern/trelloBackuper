# Trello-Backuper

This container backups regularely all files and a JSON export of an trello boardm into a zip-archive

add the following lines to your docker-compose.yml:

	  trelloloader:
	    build: /srv/docker/trelloLoader/src/
	    volumes:
	     - /srv/docker/trelloLoader/data:/data/
	    environment:
	     - TRELLO_KEY=<YOUR_API_KEY>
	     - TRELLO_TOKEN=<YOUR_API_TOKEN>
	     - TRELLO_BOARD=<YOUR_BOARD_ID>

In order to get your board ID run:

	docker-compose run --rm trelloloader nodejs /root/download.js

Finaly run docker-compose up -d trelloloader

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" href="http://purl.org/dc/dcmitype/StillImage" property="dct:title" rel="dct:type">Trello-Backuper</span> von <span xmlns:cc="http://creativecommons.org/ns#" property="cc:attributionName">J. Vollhardt und J. Braun</span> ist lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Namensnennung 4.0 International Lizenz</a>.

