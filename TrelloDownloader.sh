#!/bin/bash

# $1: Datendatei

timestamp=$(date +"%Y-%m-%d_%H-%M");
mkdir ./data
cd ./data

while read line
do
        url=`echo $line | cut -d'|' -f1`;
        destination=`echo $line | cut -d'|' -f2`;
        wget -c -P $destination $url;
done <$1

cp $2 ./

cd ./..


checksumNew=$(find ./data/ -type f -exec md5sum {} \; | md5sum | cut -d ' ' -f1);
checksumOld=$(cat lastChecksum.txt);
echo $checksumNew > lastChecksum.txt
#checksumNew=$(cat lastChecksum.txt);

#checksumNew=$(tr -dc '[[:print:]]' <<< "$checksumNew")
#checksumOld=$(tr -dc '[[:print:]]' <<< "$checksumOld")

echo $checksumNew "|";
echo $checksumOld "|";

if [[ "$checksumNew" != "$checksumOld" ]] 
then
  echo "found changes!";
  zip -r ./Backup_Trello_$timestamp.zip ./data/
  mv ./Backup_Trello_$timestamp.zip /data/
  rm /data/newest.zip;
  ln /data/Backup_Trello_$timestamp.zip /data/newest.zip;
else
  echo "nothing changed:";
fi

#rm -r ./data
