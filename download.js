var Trello = require("node-trello");

if(!process.env.hasOwnProperty("TRELLO_KEY")){
  console.log("Please set the environement variable TRELLO_KEY with your Trello-API-KEY!");
  process.exit(1);
}
if(!process.env.hasOwnProperty("TRELLO_TOKEN")){
  console.log("Please set the environement variable TRELLO_TOKEN with your Trello-API-TOKEN!");
  process.exit(1);
}

var t = new Trello(process.env.TRELLO_KEY, process.env.TRELLO_TOKEN);

var fs = require('fs');

if(process.env.hasOwnProperty("TRELLO_BOARD")){
    if(process.argc === 3 || process.argv[2] === "--json"){
        backupBoard();
    }
    else{
        loadLists(saveCards);
    }
}
else{ //show help

console.log("Please set the environement variable TRELLO_BOARD with the board id:");
console.log("Available boards: Name     <board_id>\n\r");
t.get("/1/members/me/boards", function(err, data) {
  if (err) throw err;
 data.forEach( function(d){
    console.log(d.name + "    " + d.id);
 });
});
}

function backupBoard() {
    t.get("/1/boards/" + process.env.TRELLO_BOARD + "", {
        fields: "all",
        actions: "all",
        action_fields: "all",
        actions_limit: 1000,
        cards: "all",
        card_fields: "all",
        card_attachments: true,
        labels: "all",
        lists: "all",
        list_fields: "all",
        members: "all",
        member_fields: "all",
        checklists: "all",
        checklist_fields: "all",
        organization: false
    },function(err, data) {
        console.log(data);
    });
}

var lists = [];
function loadLists(callback){
    t.get("/1/boards/" + process.env.TRELLO_BOARD + "/lists", function(err, data) {
        if (err) throw err;
        //console.log(data);
        data.forEach( function(list){
            lists[list.id] = name2filename(list.name);
        });
        callback();
    });   
}

function saveCards(){
    t.get("/1/boards/" + process.env.TRELLO_BOARD + "/cards", function(err, data) {
        if (err) throw err;
        //console.log(data);
        data.forEach( function(d){
            if(d.badges.attachments != 0){//has attachment --> load
             //console.log(d.name);
             saveCard(d);
            }
        });
    });   
}

function saveCard(card){
    t.get("/1/cards/" + card.id + "/attachments", function(err, data) {
        data.forEach(function(attachment){
           console.log(attachment.url + "|" + lists[card.idList]  + "/" + name2filename(card.name) + "/");  
        });
    });
}

function name2filename(str){
        str = str.replace(/\s/g,"_");
        
        str = str.replace(/�/g,"ae");
        str = str.replace(/ö/g,"oe");
        str = str.replace(/ü/g,"ue");
        
        str = str.replace(/Ä/g,"Ae");
        str = str.replace(/Ö/g,"Oe");
        str = str.replace(/Ü/g,"Ue");
        
        str = str.replace(/ß/g,"ss");
        
        str = str.replace(/\?/g,"");
        str = str.replace(/\//g,"");
        str = str.replace(/!/g,"");
        return str;
}

